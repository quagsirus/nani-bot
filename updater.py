# Imports required libraries
try:
    import os
    import git
except ModuleNotFoundError:
    print('FATAL: Not all libraries were found')
    import sys
    sys.exit()

intended_wd = '/opt/python/nani'

sendtonull = '>/dev/null 2>&1'

# Changes directory
if os.getcwd() != intended_wd:
    try:
        os.chdir(intended_wd)
        print('DEBUG: Changed to intended directory', intended_wd)
    except FileNotFoundError:
        print('FATAL: Not running on intended server / folder not found')
        import sys
        sys.exit()

# Pulls latest commits from remote)
g = git.Repo().remotes.origin
g.pull()
print('INFO: Pulled latest changes from remote')

# Adds/Updates libraries
#os.system('sudo pip install -U -r requirements.txt' + sendtonull)
#print('INFO: Updated pip libraries')

# Reloads bot
os.system('sudo systemctl restart nani')
print('INFO: Reloaded bot')
