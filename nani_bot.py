import discord
from discord import voice_client
from discord.ext import commands, tasks
from discord.voice_client import VoiceClient
from discord.ext.commands import bot
from discord.utils import get
from random import randint
from time import sleep
import datetime
from urllib import parse, request
import re
import yaml
from datetime import date
import copy
import time
from copy import deepcopy
import os
import youtube_dl 



intents = discord.Intents.default()
intents.members = True
intents.messages = True
intents.guilds = True

bot = commands.Bot(command_prefix='.', activity=discord.Activity(type=3, name='my goth wife'), intents=intents)
bot.remove_command('help')

   # Change 'type=X' to change discord message
    # 1 / 0 = Playing
    # 2 = Listening
    # 3 = Watching
    # 4 = Nothing

def load_global(author=None, others=None):
    involved_members = [author]
    if others and others != [None]:
        involved_members.extend(others)  # Creates list of people to update data for
    try:
        global_data = yaml.safe_load(open('global_data.yml'))  # Loads dictionary from file
    except FileNotFoundError:
        global_data = {}  # Creates an empty dictionary if file didn't exist
        save_global(global_data)
    if involved_members != [None]:
        for user in involved_members:
            if user.id not in global_data:
                global_data[user.id] = {}  # Creates member profile if user didn't exist
            for key in ['simp','simp_votes','size']:
                if key not in global_data[user.id]:  # Makes sure all keys are present for all users
                    # Re-generates user data from scratch
                    old_user_data = copy.deepcopy(global_data[user.id])
                    global_data[user.id] = {}
                    global_data[user.id]['simp'] = 0
                    global_data[user.id]['simp_votes'] = 5
                    global_data[user.id]['size'] = randint(1,12)
                    # Replaces new values with old ones if they existed
                    global_data[user.id].update(old_user_data)
                    save_global(global_data)
                    break
        del global_data['token']
    return global_data

def save_global(global_data_saving):
    global_data_saving = deepcopy(global_data_saving)
    if 'bot_token' not in globals():
        global_data_saving['token'] = 'PUT_YOUR_TOKEN_HERE'
    else:
        global_data_saving['token'] = bot_token
    with open('global_data.yml', 'w') as f:
        yaml.dump(global_data_saving,f)

@bot.event
async def on_ready():
    print("started successfully!")

@bot.event
async def on_message(message, timeout=10,):
    if message.author != bot.user:
        if "nvm" in message.content:
            await message.channel.send('https://media.discordapp.net/attachments/646459158341615628/818087843284647936/nvm.gif')
    await bot.process_commands(message)

# on member join
@bot.event
async def on_member_join(member):
    embed = discord.Embed(title = "Welcome to catpowered", description=f"You are member {len(list(member.guild.members))} of catpowered.", colour=discord.Colour(0xE606DF))
    embed.set_thumbnail(url=f"{member.avatar_url}")
    embed.set_author(name=f"{member.name}", icon_url=f"{member.avatar_url}")
    embed.set_footer(text=f"{member.guild}", icon_url=f"{member.guild.icon_url}")
    embed.timestamp = datetime.datetime.utcnow()
    channel = bot.get_channel(id=608043809338687488)
    await channel.send(embed=embed)

# .info
@bot.command()
async def info(ctx):
    if ctx.message.author.id in [274926795285987328 , 274209973196816385]:
        embed = discord.Embed(title = "a crumb of info", description="ask the owner for more", colour=discord.Colour(0xE606DF))
        embed.add_field(name="Server Owner", value=f"{ctx.guild.owner}")
        embed.add_field(name="Server Region", value=f"{ctx.guild.region}")
        embed.add_field(name="Bot Owner", value=f"Apollo#6922")
        await ctx.send(embed=embed)
    else:
        await ctx.send("You do not have permission to send this command.")

# .help
@bot.command()
async def help(ctx):
    embed = discord.Embed(title = "command List", description="all homemade ;)", colour=discord.Colour(0xE606DF))
    embed.add_field(name="Want help?", value="DM someone who looks important, they might help ;)")
    await ctx.send(embed=embed)


# .off
@bot.command()
async def off(ctx):
    if await bot.is_owner(ctx.message.author):
        await bot.change_presence(status=discord.Status.offline)
        await bot.logout()


# .hello
@bot.command()
async def hello(ctx):
    await ctx.send('Yes...')


# .vibe
@bot.command()
async def vibe(ctx):
    x = randint(1,100)
    a = randint(1,100)
    
    if ctx.message.author.id == 274926795285987328:
        await ctx.send('Wow ' + ctx.message.author.mention + ' is vibin!')
    elif ctx.message.author.id == 274234903116382208:
        await ctx.send(ctx.message.author.mention + ' is not vibin!')
    else:
        if x >= a:
            await ctx.send('Wow ' + ctx.message.author.mention + ' is vibin!')
        else:
            await ctx.send(ctx.message.author.mention + ' is not vibin!')


# .size
@bot.command()
async def size(ctx):
    global_data = load_global(ctx.message.author)
    
    joe = global_data[ctx.message.author.id]['size']
    await ctx.send(ctx.message.author.name + "'s PP is " + str(joe) + " inches long.")

# .simp
@bot.command()
async def simp(ctx, accused: discord.Member=None):
    global_data = load_global(ctx.message.author, others=[accused])

    if accused:
        if global_data[ctx.message.author.id]['simp_votes'] >0:
            global_data[ctx.message.author.id]['simp_votes'] -= 1
            global_data[accused.id]['simp'] += 1
        else:
            await ctx.send("You have run out of votes.")
        save_global(global_data)
    
    guild = ctx.message.author.guild
    embed = discord.Embed(title = "simp leaderboard", description="all homemade ;)", colour=discord.Colour(0xE606DF))
    for user in global_data:
        try:
            if global_data[user]['simp'] != 0:
                embed.add_field(name=guild.get_member(user).display_name, value=global_data[user]['simp'])
        except (AttributeError, KeyError):
            pass # If user has left the discord server or doesn't have simp value
    await ctx.send(embed=embed)


# .day
@bot.command()
async def day(ctx):
    today = date.today()
    # The time is 1 hour behind // probably from DST
    rare_video = randint(1,25)
    if rare_video == 1:
        await ctx.send("https://cdn.catpowered.net/discord/day/rare.mp4")
    else:
        await ctx.send("https://cdn.catpowered.net/discord/day/{day}.mp4".format(day=today.strftime("%A").lower()))


# .dailycat
@bot.command()
async def dailycat(ctx):
    today = date.today()
    # The time is 1 hour behind // probably from DST
    rare_video = randint(1,25)
    if rare_video == 1:
        await ctx.send("https://cdn.catpowered.net/discord/day/cat/rare.png")
    else:
        await ctx.send("https://cdn.catpowered.net/discord/day/cat/{day}.png".format(day=today.strftime("%A").lower()))


# .dub
@bot.command()
async def dub(ctx):
    random = randint(1,38)
    await ctx.send("https://cdn.catpowered.net/discord/dub/{number}.jpg".format(number=random))


# .connection
@bot.command()
async def connection(ctx):
    ping = str(round(bot.latency*1000, 1)) + 'ms'
    await ctx.send("My connection is "+ ping + ".")


# .bruh
@bot.command()
async def bruh(ctx):
    await ctx.send("𝙱𝚁𝚄𝙷 𝚍𝚎𝚝𝚎𝚌𝚝𝚎𝚍, 𝚊𝚌𝚝𝚒𝚟𝚊𝚝𝚒𝚗𝚐 𝚠𝚊𝚛𝚗𝚒𝚗𝚐 𝚜𝚒𝚐𝚗𝚜... ⚠️\n𝙿𝚕𝚎𝚊𝚜𝚎 𝚛𝚎𝚖𝚊𝚒𝚗 𝚌𝚊𝚕𝚖𝚎𝚍 𝚠𝚑𝚒𝚕𝚎 𝚐𝚘𝚒𝚗𝚐 𝚝𝚘 𝚝𝚑𝚎 𝚗𝚎𝚊𝚛𝚎𝚜𝚝 𝚎𝚟𝚊𝚌𝚞𝚊𝚝𝚒𝚘𝚗 𝚣𝚘𝚗𝚎. 🚸➡️☣️\n𝙰𝚃𝚃𝙴𝙽𝚃𝙸𝙾𝙽 𝚃𝙷𝙸𝚂 𝙸𝚂 𝙽𝙾𝚃 𝙰 𝙳𝚁𝙸𝙻𝙻 ⚠️⚠️⚠️\n𝚃𝚑𝚒𝚜 𝚒𝚜 𝚊 𝙱𝚁𝚄𝙷 𝙰𝙻𝙴𝚁𝚃, 𝚎𝚟𝚎𝚛𝚢 𝚙𝚎𝚛𝚜𝚘𝚗𝚗𝚎𝚕 𝚝𝚘 𝚢𝚘𝚞𝚛 𝚎𝚜𝚌𝚊𝚙𝚎 𝚙𝚘𝚍𝚜, 𝚝𝚑𝚒𝚜 𝚒𝚜 𝚗𝚘𝚝 𝚊 𝚍𝚛𝚒𝚕𝚕 🚻➡️🚀")


# .bonk
@bot.command(pass_context=True)
async def bonk(ctx):
    channel = ctx.message.author.voice.channel
    voice = get(bot.voice_clients, guild=ctx.guild)
    if voice and voice.is_connected():
        await voice.move_to(channel)
    else:
        voice = await channel.connect()
    await ctx.send("Stop being horny")
    voice.play(discord.FFmpegPCMAudio("bonk.mp3"))
    while voice.is_playing(): time.sleep(1)
    await ctx.message.guild.voice_client.disconnect()

# .alvin
@bot.command(pass_context=True)
async def alvin(ctx):
    channel = ctx.message.author.voice.channel
    voice = get(bot.voice_clients, guild=ctx.guild)
    if voice and voice.is_connected():
        await voice.move_to(channel)
    else:
        voice = await channel.connect()
    voice.play(discord.FFmpegPCMAudio("alvin.mp3"))
    while voice.is_playing(): time.sleep(1)
    await ctx.message.guild.voice_client.disconnect()


# .smash
@bot.command()
async def smash(ctx):
    if ctx.message.author.id in [245613758536351744]:  # Caspar
        pass
    else:
        await bot.change_presence(activity=discord.Activity(type=2, name='All Star - Smash Mouth'))
        smash = ['Somebody once told me the world is gonna roll me',
'I aint the sharpest tool in the shed',
'She was looking kind of dumb with her finger and her thumb',
'In the shape of an "L" on her forehead',
'Well, the years start coming and they dont stop coming',
'Fed to the rules and I hit the ground running',
'Didnt make sense not to live for fun',
'Your brain gets smart but your head gets dumb',
'So much to do, so much to see',
'So whats wrong with taking the back streets?',
'Youll never know if you dont go',
'Youll never shine if you dont glow',
'Hey, now, youre an all-star, get your game on, go play',
'Hey, now, youre a rock star, get the show on, get paid',
'And all that glitters is gold',
'Only shooting stars break the mold',
'Its a cool place and they say it gets colder',
'Youre bundled up now wait til you get older',
'But the meteor men beg to differ',
'Judging by the hole in the satellite picture',
'The ice we skate is getting pretty thin',
'The waters getting warm so you might as well swim',
'My worlds on fire. How about yours?',
'Thats the way I like it and Ill never get bored',
'Hey, now, youre an all-star, get your game on, go play',
'Hey, now, youre a rock star, get the show on, get paid',
'And all that glitters is gold',
'Only shooting stars break the mold',
'Go for the moon',
'Go for the moon',
'Go for the moon',
'Go for the moon',
'Hey, now, youre an all-star, get your game on, go play',
'Hey, now, youre a rock star, get the show on, get paid',
'And all that glitters is gold',
'Only shooting stars',
'Somebody once asked could I spare some change for gas',
'I need to get myself away from this place',
'I said yep, what a concept',
'I could use a little fuel myself',
'And we could all use a little change',
'Well, the years start coming and they dont stop coming',
'Fed to the rules and I hit the ground running',
'Didnt make sense not to live for fun',
'Your brain gets smart but your head gets dumb',
'So much to do, so much to see',
'So whats wrong with taking the back streets?',
'Youll never know if you dont go',
'Youll never shine if you dont glow',
'Hey, now, youre an all star, get your game on, go play',
'Hey, now, youre a rock star, get the show on, get paid',
'And all that glitters is gold',
'Only shooting stars break the mold',
'And all that glitters is gold',
'Only shooting stars break the mold'] 

        count = 0
        while True:
            try:
                await ctx.send(smash[count])
            except IndexError:
                break
            sleep(0.75)
            count += 1
        await bot.change_presence(activity=discord.Activity(type=0, name='with my robo junk'))

try:
    bot_token = load_global()['token']
    if bot_token == 'PUT_YOUR_TOKEN_HERE':
        raise KeyError
    print('starting bot...')
    bot.run(bot_token)
except KeyError:
    print('add bot token to global_data.yml please')


# everything below this point probably does not work and might break everything >3
# .play (a song)
async def play(ctx, url):
    try:
        server = ctx.message.guild
        voice_channel = server.voice_client

        async with ctx.typing():
            filename = await YTDLSorce.from_url(url, loop=bot.loop)
            voice_channel.play(discord.FFmpegPCMAudio (executable="ffmpeg.exe",source=filename))
        await ctx.send('Now playing {}'.format(filename))
    except:
        await ctx.send('The bot is not connected to a voice channel :(')

# .stop
async def stop(ctx):
        voice_client = ctx.message.guild.voice_client
        if voice_client.is_playing():
            await voice_client.stop()
        else:
            await ctx.send('The bot is not playing something at this time :(')
